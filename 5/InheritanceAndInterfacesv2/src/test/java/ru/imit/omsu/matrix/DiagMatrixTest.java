package ru.imit.omsu.matrix;

import org.junit.Test;

import static org.junit.Assert.*;

public class DiagMatrixTest {

    @Test
    public void constructor1(){
        double [] arr = new double[]{1,5,10};

        DiagMatrix test = new DiagMatrix(arr);
    }

    @Test
    public void constructor2(){
        DiagMatrix test = new DiagMatrix(3);
        test.setElem(0,0,1);
        test.setElem(1,1,2);
        test.setElem(2,2,3);

        assertEquals(3, test.getElem(2,2), 0.001);

        assertEquals(6, test.determinant(), 0.001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setElem() {
        double [] arr = new double[]{1,5,9};
        DiagMatrix test = new DiagMatrix(arr);
        // 1 0 0
        // 0 5 0
        // 0 0 9
        test.setElem(1, 2, 1323);
    }

    @Test
    public void determinant() {
        double [] arr = new double[]{1,5,10};
        DiagMatrix test = new DiagMatrix(arr);

        assertEquals(50, test.determinant(), 0.001);
    }
}