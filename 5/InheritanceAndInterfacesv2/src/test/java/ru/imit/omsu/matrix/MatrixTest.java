package ru.imit.omsu.matrix;

import org.junit.Test;

import static org.junit.Assert.*;

//https://matrixcalc.org/
public class MatrixTest {

    @Test(expected = IllegalArgumentException.class)
    public void create() {
        double [] arr = new double[]{1,2,3,4,5,6,7,8};
        Matrix test = new Matrix(arr);
    }

    @Test
    public void determinant() {
        double [] arr = new double[]{1,2,3,4,5,6,7,8,10};
        double [] arr1 = new double[]{1,152,3,14,5,6,7,8,10};
        Matrix test = new Matrix(arr);
        Matrix test1 = new Matrix(arr1);

        assertEquals(-3 ,test.determinant(),0.1);
        assertEquals(-14663,test1.determinant(),0.1);

        assertEquals(-3 ,test.determinant(),0.1);//Проверка сохранения определителя матрицы
        test.setElem(1,0,0);

        assertEquals(-19 ,test.determinant(),0.1);//Проверка после изменения элемента в матрице
    }

    @Test
    public void getAndSetElem(){
        double [] arr = new double[]{1,2,3,4,5,6,7,8,10};
        Matrix test = new Matrix(arr);
        assertEquals(1, test.getElem(0,0), 0.1);
        assertEquals(4, test.getElem(1,0), 0.1);
        assertEquals(10, test.getElem(2,2), 0.1);

        test.setElem(1,1,12400);

        assertEquals(12400, test.getElem(1,1), 0.1);
    }


}