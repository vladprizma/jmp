package ru.imit.omsu.matrix;

import org.junit.Test;

import static org.junit.Assert.*;

public class UpTriangleMatrixTest {

    @Test
    public void constructor1(){
        double [] arr = new double[]{1,2,3,5,6,10};

        UpTriangleMatrix test = new UpTriangleMatrix(arr);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setElem(){
        double [] arr = new double[]{1,2,3,5,6,10};

        UpTriangleMatrix test = new UpTriangleMatrix(arr);
        test.setElem(1,0,5);

    }

    @Test
    public void setElem2(){
        double [] arr = new double[]{1,2,3,5,6,10};

        UpTriangleMatrix test = new UpTriangleMatrix(arr);

        test.setElem(1,2,5);
        assertEquals(10, test.getElem(2,2), 0.1);
    }

    @Test
    public void determinant() {
        double [] arr = new double[]{1,0,0,5,0,10};
        UpTriangleMatrix test = new UpTriangleMatrix(arr);

        assertEquals(50, test.determinant(), 0.001);
    }

}