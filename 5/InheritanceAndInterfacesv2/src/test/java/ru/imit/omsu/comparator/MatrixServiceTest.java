package ru.imit.omsu.comparator;

import ru.imit.omsu.matrix.Matrix;
import org.junit.Test;

import static org.junit.Assert.*;

public class MatrixServiceTest {

    @Test
    public void sortMatrices() {
        double[] arr = new double[]{1, 2, 3, 4, 5, 6, 7, 8, 10};//-3
        double[] arr1 = new double[]{1, 152, 3, 14, 5, 6, 7, 8, 10};//-14663
        double[] arr2 = new double[]{1, 2, 3, 4, 5, 6, 7, 8, 14};//-15

        Matrix [] test = {new Matrix(arr),new Matrix(arr1),new Matrix(arr2)};
        Matrix [] test2 = {new Matrix(arr1),new Matrix(arr2),new Matrix(arr)};

        MatrixService.arrangeMatrices(test, new MatrixComparator<Matrix>());

        assertEquals(0, new MatrixComparator<Matrix>().compare(test2[0],test[0]));
        assertEquals(0, new MatrixComparator<Matrix>().compare(test2[1],test[1]));
        assertEquals(0, new MatrixComparator<Matrix>().compare(test2[2],test[2]));
    }

}