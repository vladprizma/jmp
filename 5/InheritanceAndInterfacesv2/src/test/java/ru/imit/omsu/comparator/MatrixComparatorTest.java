package ru.imit.omsu.comparator;

import ru.imit.omsu.matrix.Matrix;
import org.junit.Test;

import static org.junit.Assert.*;

public class MatrixComparatorTest {
    @Test
    public void compareSame() {
        double[] arr = new double[]{1, 2, 3, 4, 5, 6, 7, 8, 10};
        double[] arr1 = new double[]{1, 2, 3, 4, 5, 6, 7, 8, 10};
        Matrix test = new Matrix(arr);
        Matrix test1 = new Matrix(arr1);
        assertEquals(0, new MatrixComparator<Matrix>().compare(test,test1), 0.1);
    }

    @Test
    public void compareNotSame() {
        double[] arr = new double[]{1, 2, 3, 4, 5, 6, 7, 8, 10};
        double [] arr1 = new double[]{1,152,3,14,5,6,7,8,10};
        Matrix test = new Matrix(arr);// -3
        Matrix test1 = new Matrix(arr1);// -14663
        assertEquals(1, new MatrixComparator<Matrix>().compare(test,test1), 0.1);
    }
}