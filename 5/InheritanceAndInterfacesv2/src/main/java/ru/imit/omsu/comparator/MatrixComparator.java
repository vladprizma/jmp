package ru.imit.omsu.comparator;

import ru.imit.omsu.matrix.IMatrix;

import java.util.Comparator;

public class MatrixComparator <M extends IMatrix> implements Comparator<M> {
    private static final double EPS = 1E-9;
    @Override
    public int compare(M o1 , M o2) {
        double count = o1.determinant() - o2.determinant();
         if(count > EPS){
             return 1;
         }
         if(count < -EPS){
             return -1;
         }
         return 0;
    }
}
//Tests