package ru.imit.omsu.comparator;

import ru.imit.omsu.matrix.IMatrix;

import java.util.Arrays;

public class MatrixService {
    public static <M extends IMatrix> void arrangeMatrices(M [] arr, MatrixComparator<M> comparator){
        Arrays.sort(arr,comparator);
    }
}
//Tests