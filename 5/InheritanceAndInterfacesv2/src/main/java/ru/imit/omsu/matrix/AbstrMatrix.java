package ru.imit.omsu.matrix;

public  abstract class AbstrMatrix implements IMatrix{
    protected static final double EPS = 1E-9;
    protected double []matrix;
    protected int N;
    protected double bufDeter;
    protected boolean deterIsCounted;

}
