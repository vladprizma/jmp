package ru.imit.omsu.matrix;

import java.util.Arrays;
import java.util.Objects;

public class Matrix extends AbstrMatrix {

    public Matrix(int N){
        if(N <= 0){
            throw new IllegalArgumentException("Incorrect matrix dimension");
        }
        this.N = N;

        matrix = new double[N*N];
        deterIsCounted = false;
        Arrays.fill(matrix, 0);
    }

    public Matrix(double [] arr, int N){
        if(arr == null || arr.length == 0 || N == 0 ){
            throw new IllegalArgumentException("Incorrect parameters");
        }
        if((arr.length/N) != N){
            throw new IllegalArgumentException("Incorrect matrix dimension");
        }
        this.N = N;

        matrix = new double[N*N];
        deterIsCounted = false;
        for(int i = 0; i < N*N; i++){
            matrix[i] = arr[i];
        }
    }

    public Matrix(double [] arr){
        this.N = (int) Math.sqrt(arr.length);
        if (N * N != arr.length) {
            throw new IllegalArgumentException("Incorrect length");
        }

        matrix = new double[N*N];
        deterIsCounted = false;
        for(int i = 0; i < N*N; i++){
            matrix[i] = arr[i];
        }
    }

    //i - ходит по строкам
    //j - ходит по столбцам

    public void setElem(int i, int j, double element) {
        deterIsCounted = false;
        matrix[(N*i)+j] = element;
    }

    public double getElem(int i, int j) {
        return matrix[(N*i)+j];
    }

    public double determinant() {
        double determinant = 1;
        if(deterIsCounted){
            return bufDeter;
        }

        double [] copy = new double[N*N];
        for(int i = 0; i < N*N; i++){
            copy[i] = matrix[i];
        }
        double buf = 1;
        for(int i = 0; i < N-1; i++){
                buf=1;
                for(int k = i + 1 ; k < N;k++) {

                    buf = Math.abs(copy[(N * k) + i])/Math.abs(copy[(N * i) + i]);
                    //Проверка знаков
                    int m = 1;
                    if ((copy[(N * k) + i] < EPS && copy[(N * i) + i] < EPS) ||
                            (copy[(N * k) + i] > EPS && copy[(N * i) + i] > EPS)) {
                        m = -1;
                    }
                    //вычитаем текущую строку с нижними, для зануления столбца под элементом из главной диагонали
                    if (m * (buf * copy[(N * i) + i]) + copy[(N * k) + i] < EPS) {
                        for(int j = i; j < N; j++){
                            copy[(N * k) + j] += m * (buf * copy[(N * i) + j]);
                        }
                    }
                }

        }

        for(int i = 0; i < N*N; i+= N+1){
            determinant *= copy[i];
        }
        bufDeter = determinant;
        deterIsCounted = true;
        return determinant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Matrix matrix1 = (Matrix) o;
        return N == matrix1.N &&
                Arrays.equals(matrix, matrix1.matrix);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(N);
        result = 31 * result + Arrays.hashCode(matrix);
        return result;
    }
}

//описание
/*  До N*N элемента доходить нет смысла, т.к. ниже него(последнего элемента главной диагонали) уже ничего нет (объяснение ниже)
        for(int i = 0; i < N-1; i++){// N-1 т.к. мы из N строки вычитаем N-1 (всегда), а вычесть (N+1 - N) невозможно, ведь у нас нет N+1 строки
                buf=1;
                for(int k = i + 1 ; k < N;k++) {
                    //Нам важно отношение между нашим элементом i,j и элементом i, j+1 ((i, j+1)/(i,j))
                    //Ибо на него мы будем умножать i,j, чтобы занулить i, j+1
                    buf = Math.abs(copy[(N * k) + i])/Math.abs(copy[(N * i) + i]);
                    //Проверка знаков
                    int m = 1;
                    if ((copy[(N * k) + i] < EPS && copy[(N * i) + i] < EPS) ||
                            (copy[(N * k) + i] > EPS && copy[(N * i) + i] > EPS)) {
                        m = -1;
                    }
                    //вычитаем текущую строку с нижними, для зануления столбца под элементом из главной диагонали
                    if (m * (buf * copy[(N * i) + i]) + copy[(N * k) + i] < EPS) {
                        for(int j = i; j < N; j++){
                            copy[(N * k) + j] += m * (buf * copy[(N * i) + j]);
                        }
                    } else {
                        throw new IllegalArgumentException("Unknown problem");
                    }
                }

        }*/