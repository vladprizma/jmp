package ru.imit.omsu.matrix;

public class UpTriangleMatrix extends AbstrMatrix {

    public UpTriangleMatrix(double [] arr){
        if(arr == null || arr.length == 0){
            throw new IllegalArgumentException("Incorrect parameters");
        }
        double discriminant = Math.sqrt(1 + 8*arr.length);
        if(discriminant - (int) discriminant != 0){
            throw new IllegalArgumentException("Incorrect data insertion attempt");
        }
        this.matrix = new double[arr.length];
        this.deterIsCounted = false;
        this.N = (int) (-0.5 + discriminant/2);
        int count = 0;

        for(double elem : arr){
            this.matrix[count] = elem;
            count++;
        }
    }

    //i*(i+1)/2 - это погрешность от того что матрица на самом деле не квадратная
    public double getElem(int i, int j){
        if(j >= i){
            return matrix[((i * N) + j) - i*(i+1)/2];
        }
        return 0;
    }

    public void setElem(int i, int j, double element) {
        if (i < 0 || j < 0 || i > matrix.length || j > matrix.length || Double.compare(element, 0.0) != 0 && j < i) {
            throw new IllegalArgumentException("Incorrect data insertion attempt");
        }
        if(j >= i) {
            matrix[((i * N) + j) - i * (i + 1) / 2] = element;
            deterIsCounted = false;
        }
    }

    public double determinant() {
        double determinant = 1;
        if(deterIsCounted){
            return bufDeter;
        }
        int difference = this.N;// - это погрешность от того что матрица на самом деле не квадратная

        for(int i = 0; i < matrix.length; i+= difference--){
            determinant *= matrix[i];
        }

        bufDeter = determinant;
        deterIsCounted = true;
        return determinant;
    }
}