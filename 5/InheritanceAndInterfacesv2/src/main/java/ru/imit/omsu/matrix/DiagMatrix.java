package ru.imit.omsu.matrix;

import java.util.Arrays;

public class DiagMatrix extends AbstrMatrix {

    public DiagMatrix(int N){
        if(N <= 0){
            throw new IllegalArgumentException("Incorrect length");
        }
        this.N = N;
        this.matrix = new double[N];
        this.deterIsCounted = false;
        Arrays.fill(matrix, 0);
    }

    // На вход подается только диагональ
    public DiagMatrix(double [] arr) {
        if(arr == null || arr.length == 0){
            throw new IllegalArgumentException("Incorrect parameters");
        }
        this.matrix = new double[arr.length];
        this.N = matrix.length;
        this.deterIsCounted = false;
        int count = 0;

        for(double elem : arr){
            this.matrix[count] = elem;
            count++;
        }
    }

    public double getElem(int i, int j){
        if(i == j){
            return matrix[i];
        }
        return 0;
    }

    public void setElem(int i, int j, double element) {
        if(i < 0 || j < 0 || i > matrix.length || j > matrix.length || i != j && element != 0){
            throw new IllegalArgumentException("Incorrect data insertion attempt");
        }
        if(i == j) {
            matrix[i] = element;
            deterIsCounted = false;
        }
    }

    public double determinant() {
        double determinant = 1;
        if(deterIsCounted){
            return bufDeter;
        }

        for(int i = 0; i < N; i++){
            determinant *= matrix[i];
        }

        bufDeter = determinant;
        deterIsCounted = true;
        return determinant;
    }
}