

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Person implements Serializable {
    private String firstName;
    private String secondName;
    private String patronymic;
    private Date birth;

    public Person(){
        this.firstName = "";
        this.secondName = "";
        this.patronymic = "";
        this.birth = new Date();
    }

    public Person(String firstName, String secondName, String patronymic, Date birth){
        this.firstName = firstName.trim();
        this.secondName = secondName.trim();
        this.patronymic = patronymic.trim();
        this.birth = (Date) birth.clone();
    }

    public Person(Person person){
        this(person.firstName, person.secondName,person.patronymic,person.birth);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public Date getBirth() {
        return birth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(firstName, person.firstName) &&
                Objects.equals(secondName, person.secondName) &&
                Objects.equals(patronymic, person.patronymic) &&
                Objects.equals(birth, person.birth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, secondName, patronymic, birth);
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", birth=" + birth +
                '}';
    }
}
