import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;

class ServiceClass implements Serializable {

    static void serialize(House house, String file) throws IOException {
        ObjectOutput out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
        out.writeObject(house);
        out.flush();
    }

    static House deserialize(String file) throws IOException, ClassNotFoundException {
        ObjectInput in = new ObjectInputStream( new BufferedInputStream( new FileInputStream(file)));
        return (House) in.readObject();
    }

    static String serializeJSON(House house) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(house);
    }

    static House deserializeJSON(String jsonString) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsonString, House.class);
    }

}
