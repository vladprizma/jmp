import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class House implements Serializable {
    private String number;
    private String address;
    private Person housekeeper;
    private List<Flat> flats;

    public House(){
        this.number = "";
        this.address = "";
        this.housekeeper = new Person();
        this.flats = new ArrayList<>();
    }

    public House(String number, String address, Person housekeeper, List<Flat> flats){
        this.number = number.trim();
        this.address = address.trim();
        this.housekeeper = new Person();
        this.flats = new ArrayList<>();
    }

    public House(House house){
        this(house.number,house.address,house.housekeeper,house.flats);
    }

    public String getNumber() {
        return number;
    }

    public String getAddress() {
        return address;
    }

    public Person getHousekeeper() {
        return housekeeper;
    }

    public List<Flat> getFlats() {
        return flats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        House house = (House) o;
        return Objects.equals(number, house.number) &&
                Objects.equals(address, house.address) &&
                Objects.equals(housekeeper, house.housekeeper) &&
                Objects.equals(flats, house.flats);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, address, housekeeper, flats);
    }

    @Override
    public String toString() {
        return "House{" +
                "number='" + number + '\'' +
                ", address='" + address + '\'' +
                ", housekeeper=" + housekeeper +
                ", flats=" + flats +
                '}';
    }
}
