import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class SomeTasks {

    public static void writeStreamTestByte(OutputStream outStream, int [] arr) throws IOException {
        int[] copy = new int[arr.length];

        BufferedOutputStream out = new BufferedOutputStream(outStream);
        for (int elem : arr) {
            out.write(elem);
        }
        out.close();
    }

    public static void readStreamTestByte(InputStream inStream, int [] arr) throws IOException {
        BufferedInputStream in = new BufferedInputStream(inStream);
        int buf = 0;
        for(int i = 0; i < arr.length; i++){
            buf = in.read();
            if (buf == -1) {
                return;
            }
            arr[i] = buf;
        }
        in.close();
    }

    public static CharArrayWriter writeStreamTestChar(int [] arr) throws IOException {
        int[] copy = new int[arr.length];

        CharArrayWriter out = new CharArrayWriter();
        for (int elem : arr) {
            out.write(elem);
        }
        return out;
    }

    public static void readStreamTestChar(CharArrayWriter data, int [] arr) throws IOException {
        CharArrayReader in = new CharArrayReader(data.toCharArray());
        int buf = 0;
        for(int i = 0; i < arr.length; i++){
            if ((buf = in.read()) == -1) {
                return;
            }
            arr[i] = (char) buf;
        }
        in.close();
    }

    public static String streamTestRandom(String file, long pos) throws IOException {
        StringBuilder buf = new StringBuilder();
        int check;
        RandomAccessFile fileRead = new RandomAccessFile(file,"r");
        fileRead.seek(pos);
        check = fileRead.read();
        while (check != -1) {
            buf.append((char) check);
            check = fileRead.read();
        }
        fileRead.close();
        return new String(buf);
    }

    public static List<File> streamTestFile(String directory, String fileExpansion){
        List<File> buf = new ArrayList<>();

        File search = new File(directory);
        if(!search.isDirectory()) {
            throw new IllegalArgumentException("Given path doesn't contain catalog");
        }

        File [] dirFiles = search.listFiles();
        for(int i = 0; i < dirFiles.length; i++){
            if(dirFiles[i].getName().endsWith(fileExpansion)){
                buf.add(dirFiles[i]);
            }
        }

        return buf;
    }

}
