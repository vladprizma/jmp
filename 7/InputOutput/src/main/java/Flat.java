import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

public class Flat implements Serializable {
    private int number;
    private int area;
    private Person [] persons;

    public Flat(){
        this.number = 0;
        this.area = 0;
        this.persons = new Person[1];
        this.persons[0] = new Person();
    }

    public Flat(int number, int area, Person [] persons){
        this.number = number;
        this.area = area;
        this.persons = new Person[persons.length];
        for(int i = 0; i < persons.length; i++){
            this.persons[i] = new Person(persons[i]);
        }
    }

    public Flat (Flat flat){
        this(flat.number,flat.area,flat.persons);
    }

    public int getNumber() {
        return number;
    }

    public int getArea() {
        return area;
    }

    public Person[] getPersons() {
        return persons;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flat flat = (Flat) o;
        return number == flat.number &&
                area == flat.area &&
                Arrays.equals(persons, flat.persons);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(number, area);
        result = 31 * result + Arrays.hashCode(persons);
        return result;
    }

    @Override
    public String toString() {
        return "Flat{" +
                "number=" + number +
                ", area=" + area +
                ", persons=" + Arrays.toString(persons) +
                '}';
    }
}
