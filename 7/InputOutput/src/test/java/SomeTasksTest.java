import org.junit.Test;

import java.io.*;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class SomeTasksTest {

    @Test
    public void byteIO() throws IOException {
        int [] test = {1,2,3,4,5,6};
        int [] testGet = new int[6];
        ByteArrayOutputStream buf = new ByteArrayOutputStream(6);
        SomeTasks.writeStreamTestByte(buf,test);
        ByteArrayInputStream bufIn = new ByteArrayInputStream(buf.toByteArray());
        SomeTasks.readStreamTestByte(bufIn,testGet);

        assertArrayEquals(test,testGet);
    }

    @Test
    public void charIO() throws IOException {
        int [] test = {1,2,3,4,5,7};
        int [] testGet = new int[6];
        CharArrayWriter buf = new CharArrayWriter(6);
        SomeTasks.readStreamTestChar(
                SomeTasks.writeStreamTestChar(test),testGet);

        assertArrayEquals(test,testGet);
    }

    @Test
    public void randIO() throws IOException {
        String testAnswer = new String("6 7 8 9 10");

        assertEquals(testAnswer,SomeTasks.streamTestRandom("toPrint.txt", 10));
    }

    @Test
    public void fileIO() throws IOException {
        ArrayList<File> testAnswer = new ArrayList<>();
        testAnswer.add(new File("C:\\Users\\glitc\\Downloads\\ЯМПы и С++\\ЯМПы\\jmp\\7\\InputOutput\\example1.txt"));
        testAnswer.add(new File("C:\\Users\\glitc\\Downloads\\ЯМПы и С++\\ЯМПы\\jmp\\7\\InputOutput\\example2.txt"));
        testAnswer.add(new File("C:\\Users\\glitc\\Downloads\\ЯМПы и С++\\ЯМПы\\jmp\\7\\InputOutput\\toPrint.txt"));
        String directory = new String("C:\\Users\\glitc\\Downloads\\ЯМПы и С++\\ЯМПы\\jmp\\7\\InputOutput\\");
        String fileExpansion = new String(".txt");

        assertEquals(testAnswer,SomeTasks.streamTestFile(directory,fileExpansion));

    }

}