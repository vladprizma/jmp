import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class ServiceClassTest {

    @Test
    public void testSerialize() throws IOException, ClassNotFoundException {
        Person person1 = new Person("A","B","C", new Date(18381381813132L));
        Person person2 = new Person("G","Q","D", new Date(1838138182L));
        Person person3 = new Person("F","L","V", new Date(18381384141L));
        Person person4 = new Person("A","A","A", new Date(18381384143221L));

        Flat flat1 = new Flat(20,200,new Person[]{person1,person2});
        Flat flat2 = new Flat(22,200,new Person[]{person3});

        List<Flat> flats = new ArrayList<>();
        flats.add(flat1);
        flats.add(flat2);

        House test = new House("20","Buldog st.", person4, flats);

        ServiceClass.serialize(test, "example1.txt");
        House testRes = ServiceClass.deserialize("example1.txt");
        assertEquals(test, testRes);
    }

    @Test
    public void testSerializeJSON() throws IOException, ClassNotFoundException {
        Person person1 = new Person("A","B","C", new Date(18381381813132L));
        Person person2 = new Person("G","Q","D", new Date(1838138182L));
        Person person3 = new Person("F","L","V", new Date(18381384141L));
        Person person4 = new Person("A","A","A", new Date(18381384143221L));

        Flat flat1 = new Flat(20,200,new Person[]{person1,person2});
        Flat flat2 = new Flat(22,200,new Person[]{person3});

        List<Flat> flats = new ArrayList<>();
        flats.add(flat1);
        flats.add(flat2);

        House test = new House("20","Buldog st.", person4, flats);

        System.out.println(ServiceClass.serializeJSON(test));

        House testRes = ServiceClass.deserializeJSON(ServiceClass.serializeJSON(test));

        assertEquals(test, testRes);
    }

}