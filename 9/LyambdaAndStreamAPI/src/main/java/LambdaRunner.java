import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public class LambdaRunner {

    public static <T> Object strLambdaApply(String str, LambdaDemo.IOperation oper){
        return oper.apply(str);
    }

    public static <T, U> Object funcApply(T data, Function<T, U> lambda) {
        return lambda.apply(data);
    }

    public static <T, U, V> Object biFuncApply(T data1, U data2, BiFunction<T, U, V> lambda) {
        return lambda.apply(data1, data2);
    }
}
