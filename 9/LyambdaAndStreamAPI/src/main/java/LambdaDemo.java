import javax.swing.text.StyledEditorKit;
import java.util.List;
import java.util.function.*;

public class LambdaDemo {

    @FunctionalInterface
    public interface IOperation<T> {
        T apply(String str);
    }

    public static final IOperation<Integer> strLength = String::length;

    public static final Function<String, Character> strFirstChar = str -> {
        if("".equals(str)){
            return null;
        }
        return str.charAt(0);
    };

    public static final IOperation<Boolean> containsSpace = str -> str.contains(" ");

    public static final IOperation<Integer> amountOfWordsWithoutChar = str -> str.split(",").length;

    public static final Function<Human,Integer> getAge = Human::getAge;

    public static final BiFunction<Human, Human, Boolean> sameSecondName = (human1, human2) ->
            human1.getSecondName().equals(human2.getSecondName());

    public static final Function<Human, String> getFullName = human -> human.getSecondName() +
            " " + human.getFirstName() + " " + human.getPatronymic();

    public static final Function<Human, Human> newHumanAddOneAge = human -> new Human(human.getFirstName(),
            human.getSecondName(), human.getPatronymic(),human.getAge()+1, human.getGender());

    public static final BiFunction<List<Human>, Integer, Boolean> youngerThanMax = (humans, maxAge) -> {
        boolean check = true;
        for(Human elem: humans){
            if(elem.getAge() >= maxAge){
                check = false;
                break;
            }
        }
        return check;
    };

}
