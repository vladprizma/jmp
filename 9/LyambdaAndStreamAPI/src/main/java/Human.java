import java.util.Objects;

public class Human {
    private String firstName;
    private String secondName;
    private String patronymic;
    private int age;
    private Gender gender;
    public enum Gender{//с ним же нельзя сделать опасные изменения так что public
        MALE, FEMALE
    }

    public Human(String firstName, String secondName, String patronymic, int age, Gender gender) {
        this.firstName = firstName.trim();
        this.secondName = secondName.trim();
        this.patronymic = patronymic.trim();
        this.gender = gender;
        this.age = age;
    }

    public Human(Human human) {
        this(human.firstName,human.secondName, human.patronymic, human.age, human.gender);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age &&
                Objects.equals(firstName, human.firstName) &&
                Objects.equals(secondName, human.secondName) &&
                Objects.equals(patronymic, human.patronymic) &&
                gender == human.gender;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, secondName, patronymic, age, gender);
    }
}