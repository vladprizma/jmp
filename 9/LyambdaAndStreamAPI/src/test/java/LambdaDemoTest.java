import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class LambdaDemoTest {

    @Test
    public void testStrLength(){
        String test = new String("String");
        assertEquals(6, LambdaRunner.strLambdaApply(test, LambdaDemo.strLength));
    }

    @Test
    public void testFirstChar(){
        String test = new String("String");
        assertEquals('S', LambdaRunner.funcApply(test, LambdaDemo.strFirstChar));
    }
    @Test
    public void testFirstChar2(){
        String test = new String("");
        assertEquals(null, LambdaRunner.funcApply(test, LambdaDemo.strFirstChar));
    }

    @Test
    public void testContainsSpace(){
        String test = new String("Str ing");
        assertEquals(true, LambdaRunner.strLambdaApply(test, LambdaDemo.containsSpace));
    }

    @Test
    public void testAmountOfWordsWithoutChar(){
        String test = new String("St,ri,ng");
        assertEquals(3, LambdaRunner.strLambdaApply(test, LambdaDemo.amountOfWordsWithoutChar));
    }
    @Test
    public void testsGetAge(){
        Human test = new Human("A", "B", "C",28, Human.Gender.MALE);
        assertEquals(28, LambdaRunner.funcApply(test, LambdaDemo.getAge));
    }

    @Test
    public void testsGetAgeStudent(){
        Student test = new Student("A", "B", "C",28, Human.Gender.MALE,"fefef","qeqeqe","rere");
        assertEquals(28, LambdaRunner.funcApply(test, LambdaDemo.getAge));
    }

    @Test
    public void testSameSecondName(){
        Human test1 = new Human("G", "B", "Q",28, Human.Gender.MALE);
        Human test2 = new Human("A", "B", "R",35, Human.Gender.MALE);
        assertEquals(true, LambdaRunner.biFuncApply(test1,test2, LambdaDemo.sameSecondName));
    }

    @Test
    public void testSameSecondNameStudent(){
        Student test1 = new Student("G", "B", "Q",28, Human.Gender.MALE, "fefef","qeqeqe","rere");
        Student test2 = new Student("A", "B", "R",35, Human.Gender.MALE, "fefef","qeqeqe","rere");
        assertEquals(true, LambdaRunner.biFuncApply(test1,test2, LambdaDemo.sameSecondName));
    }

    @Test
    public void testGetFullName(){
        Human test = new Human("A", "B", "C",28, Human.Gender.MALE);
        assertEquals("B A C", LambdaRunner.funcApply(test, LambdaDemo.getFullName));
    }

    @Test
    public void testGetFullNameStudent(){
        Student test = new Student("A", "B", "C",28, Human.Gender.MALE, "fefef","qeqeqe","rere");
        assertEquals("B A C", LambdaRunner.funcApply(test, LambdaDemo.getFullName));
    }

    @Test
    public void testAddOneAge(){
        Human test = new Human("A", "B", "C",28, Human.Gender.MALE);
        Human testAnswer = new Human("A", "B", "C",29, Human.Gender.MALE);
        assertEquals(testAnswer, LambdaRunner.funcApply(test, LambdaDemo.newHumanAddOneAge));
    }

    @Test
    public void testYoungerThanMax(){
        Human test1 = new Human("A", "B", "C",15, Human.Gender.MALE);
        Human test2 = new Human("V", "X", "W",42, Human.Gender.MALE);
        Human test3 = new Human("A", "C", "W",29, Human.Gender.MALE);
        List<Human> test = new ArrayList<>();
        Collections.addAll(test, test1,test2, test3);

        assertEquals(true, LambdaRunner.biFuncApply(test, 43, LambdaDemo.youngerThanMax));
    }

    @Test
    public void testYoungerThanMax2(){
        Human test1 = new Human("A", "B", "C",15, Human.Gender.MALE);
        Human test2 = new Human("V", "X", "W",44, Human.Gender.MALE);
        Human test3 = new Human("A", "C", "W",29, Human.Gender.MALE);
        List<Human> test = new ArrayList<>();
        Collections.addAll(test, test1,test2, test3);
        assertEquals(false, LambdaRunner.biFuncApply(test, 43, LambdaDemo.youngerThanMax));
    }
}