package service;


import Filter.ContainsStringFilter;
import Filter.BeginStringFilter;
import Filter.IFilter;
import PackageAndProduct.*;
import org.junit.Test;

import static ServiceOfProduct.ServiceOfProduct.countByFilter;
import static ServiceOfProduct.ServiceOfProduct.countByFilterDeep;
import static ServiceOfProduct.ServiceOfProduct.checkAllWeighted;
import static org.junit.Assert.*;

public class ProductServiceTest {
    @Test
    public void countByFilterTest() {
        ProductPackage pack = new ProductPackage("simple pack ", 1) {
            @Override
            public int GetPackageWeight() {
                return 0;
            }
        };
        PackagedCatchweightGoods packProduct1 = new PackagedCatchweightGoods(new CatchweightGoods("red pickle", "weird pickle"), 2, pack);
        PackagedCatchweightGoods packProduct2 = new PackagedCatchweightGoods(new CatchweightGoods("red tomato", "weird pickle"), 2, pack);
        PackagedCatchweightGoods packProduct3 = new PackagedCatchweightGoods(new CatchweightGoods("pink pickle", "weird pickle"), 2, pack);
        PackagedCatchweightGoods packProduct4 = new PackagedCatchweightGoods(new CatchweightGoods("ice cream", "yummy ice cream "), 2, pack);
        PackagedCatchweightGoods packProduct5 = new PackagedCatchweightGoods(new CatchweightGoods("red pickle", "little pickle"), 2, pack);


        PackagedSetOfProduct packaged = new PackagedSetOfProduct(pack, "pickle box", "big box pickle", packProduct1, packProduct2, packProduct3, packProduct5);
        PackagedSetOfProduct package2 = new PackagedSetOfProduct(pack, "red box pickle box", "something yummy", packaged, packProduct2);
        Consignment consignment = new Consignment(package2, packProduct1, packProduct2, packProduct3, packProduct4, packProduct5);
        assertEquals(4, countByFilter(consignment, (IFilter) new BeginStringFilter("red")));

    }

    @Test
    public void countByFilterTestOnEmptyConsignment() {
        Consignment consignment = new Consignment();
        assertEquals(0, countByFilter(consignment, (IFilter) new BeginStringFilter("pickle")));

    }

    @Test
    public void countByFilterDeepTest() {
        //Так как набор товаров может состоять из поднабора товаров, то нужно запускать поиск в глубину
        ProductPackage pack = new ProductPackage("simple pack ", 1) {
            @Override
            public int GetPackageWeight() {
                return 0;
            }
        };
        PackagedCatchweightGoods packProduct1 = new PackagedCatchweightGoods(new CatchweightGoods("orange pickle", "weird something"), 2, pack);
        PackagedCatchweightGoods packProduct2 = new PackagedCatchweightGoods(new CatchweightGoods("pink pickle", "weird something"), 2, pack);
        PackagedCatchweightGoods packProduct3 = new PackagedCatchweightGoods(new CatchweightGoods("ice cream", "yummy ice cream "), 2, pack);
        PackagedCatchweightGoods packProduct4 = new PackagedCatchweightGoods(new CatchweightGoods("blee pickle", "simple pickle"), 2, pack);
        PackagedCatchweightGoods packProduct5 = new PackagedCatchweightGoods(new CatchweightGoods("simple pickle", "simple pickle"), 2, pack);
        //PackagedCatchweightGoods packProduct6 = new PackagedCatchweightGoods(new CatchweightGoods("simple pickle", "simple pickle"), 2, pack);
        //PackagedCatchweightGoods packProduct7 = new PackagedCatchweightGoods(new CatchweightGoods("simple pickle", "simple pickle"), 2, pack);


        PackagedSetOfProduct packaged = new PackagedSetOfProduct(pack, "pickle box", "big box pickle", packProduct3, packProduct1, packProduct2);
        PackagedSetOfProduct package2 = new PackagedSetOfProduct(pack, "big box pickle box", "something yummy", packaged, packProduct4);

        Consignment consignment = new Consignment(package2, packaged, packProduct5);
        //countByFilterDeep(consignment, new ContainsStringFilter("pickle"));
        assertEquals(9, countByFilterDeep(consignment, new ContainsStringFilter("pickle")));

    }

    @Test
    public  void checkAllWeightTestTrue(){
        ProductPackage pack = new ProductPackage("simple pack ", 1) {
            @Override
            public int GetPackageWeight() {
                return 0;
            }
        };
        PackagedCatchweightGoods packProduct1 = new PackagedCatchweightGoods(new CatchweightGoods("orange pickle", "weird something"), 2, pack);
        PackagedCatchweightGoods packProduct2 = new PackagedCatchweightGoods(new CatchweightGoods("pink pickle", "weird something"), 2, pack);
        PackagedCatchweightGoods packProduct5 = new PackagedCatchweightGoods(new CatchweightGoods("simple pickle", "simple pickle"), 2, pack);


        Consignment consignment = new Consignment(packProduct1,packProduct2,packProduct5);
        assertTrue(checkAllWeighted(consignment));

    }
    @Test
    public  void checkAllWeightTestFalse(){
        ProductPackage pack = new ProductPackage("simple pack ", 1) {
            @Override
            public int GetPackageWeight() {
                return 0;
            }
        };
        PackagedCatchweightGoods packProduct1 = new PackagedCatchweightGoods(new CatchweightGoods("orange pickle", "weird something"), 2, pack);
        PackagedCatchweightGoods packProduct2 = new PackagedCatchweightGoods(new CatchweightGoods("pink pickle", "weird something"), 2, pack);
        PackagedCatchweightGoods packProduct5 = new PackagedCatchweightGoods(new CatchweightGoods("simple pickle", "simple pickle"), 2, pack);

        PackagedPieceGoods packProduct4 = new PackagedPieceGoods(new PieceGoods("keyboard","cool keyboard",12),2,pack);


        Consignment consignment = new Consignment(/*packProduct1,packProduct2,packProduct5,*/packProduct4);
        assertFalse(checkAllWeighted(consignment));

    }
    @Test
    public  void checkAllWeightTestEmptyConsignment(){

        Consignment consignment = new Consignment();
        assertFalse(checkAllWeighted(consignment));

    }
}
