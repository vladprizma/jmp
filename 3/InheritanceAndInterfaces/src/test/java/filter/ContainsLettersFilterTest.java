package filter;

import Filter.EndStringFilter;
import Filter.IFilter;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class ContainsLettersFilterTest {
    @Test
    public  void filterTest(){
        IFilter str1 =  new EndStringFilter("раму");

        assertTrue(str1.apply("мама мыла раму"));

    }
    @Test(expected = IllegalArgumentException.class)
    public  void filterExceptionTest(){
        IFilter str1 = (IFilter) new EndStringFilter("");

        assertTrue(str1.apply("мама мыла раму"));

    }
}
