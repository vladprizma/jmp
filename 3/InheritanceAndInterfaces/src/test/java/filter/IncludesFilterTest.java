package filter;

import Filter.ContainsStringFilter;
import Filter.IFilter;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class IncludesFilterTest {
    @Test
    public  void filterTest(){
        IFilter str1 = new ContainsStringFilter("раму");

        assertTrue(str1.apply("мама мыла раму"));

    }
    @Test(expected = IllegalArgumentException.class)
    public  void filterExceptionTest(){
        IFilter str1 = new ContainsStringFilter("");

        assertTrue(str1.apply("мама мыла раму"));

    }
}
