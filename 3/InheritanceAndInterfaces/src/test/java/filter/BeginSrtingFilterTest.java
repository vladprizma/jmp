package filter;

import Filter.BeginStringFilter;
import Filter.IFilter;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class BeginSrtingFilterTest {
    @Test
    public  void filterTest(){
        IFilter str1 = new BeginStringFilter("мама");

        assertTrue(str1.apply("мама мыла раму"));

    }
    @Test(expected = IllegalArgumentException.class)
    public  void filterExceptionTest(){
        IFilter str1 = new BeginStringFilter("");

        assertTrue(str1.apply("мама мыла раму"));

    }
}
