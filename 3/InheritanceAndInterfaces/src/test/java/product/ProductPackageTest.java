package product;

import PackageAndProduct.ProductPackage;
import org.junit.Test;

public class ProductPackageTest {
    @Test(expected = IllegalArgumentException.class)
    public  void constructorTest(){
        ProductPackage object = new ProductPackage("", -15) {
            @Override
            public int GetPackageWeight() {
                return 0;
            }
        };

    }
}

