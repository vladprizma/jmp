package product;

import PackageAndProduct.*;
import org.junit.Test;

public class PackagedSetOfProductsTest {
    @Test(expected = IllegalArgumentException.class)
    public void constructorTest(){
        ProductPackage pack = new ProductPackage("name", 12);

        PackagedPieceGoods ppg = new PackagedPieceGoods( new PieceGoods("name1", "description1",22), 23,
                new ProductPackage("name12", 24));

        PackageAndProduct.PackagedCatchweightGoods pcg = new PackageAndProduct.PackagedCatchweightGoods(new CatchweightGoods("something","cool"),
                -12,new ProductPackage("cool", 15));

        PackagedSetOfProduct psop = new PackagedSetOfProduct(pack, null, "", ppg,pcg);


    }

}

//public PackagedSetOfProduct(ProductPackage pack, String name, String description, IPackedProduct... set) {
//        super(name, description);
//        this.pack = pack;
//        this.set = set;
//    }


// public PackagedPieceGoods(PieceGoods goods, int amount, ProductPackage pack) {
//        super(goods);
//        if (amount <= 0)
//            throw new IllegalArgumentException("Amount of piece goods cannot be lower 0");
//        this.amount = amount;
//        this.pack = pack;
//    }