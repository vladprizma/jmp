package product;

import PackageAndProduct.PackagedPieceGoods;
import PackageAndProduct.PieceGoods;
import PackageAndProduct.ProductPackage;
import org.junit.Test;

public class PackagedPieceGoodsTest {
    @Test(expected = IllegalArgumentException.class)
    public  void constructorTest(){
        ProductPackage pack = new ProductPackage("cool", 15) {
            @Override
            public int GetPackageWeight() {
                return 0;
            }
        };
        PieceGoods goods = new PieceGoods("box","cool",15);
        PackagedPieceGoods object = new PackagedPieceGoods(goods,-12,pack);


    }
}
