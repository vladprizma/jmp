package product;

import Filter.ContainsStringFilter;
import Filter.EndStringFilter;
import PackageAndProduct.*;
import ServiceOfProduct.ServiceOfProduct;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConsignmentTest {
    //Создать SetOfProduct в котором SetOfProduct
    @Test
    public void consignment(){

        ProductPackage pack = new ProductPackage("simple pack ", 1);
        PackagedCatchweightGoods packProduct1 = new PackagedCatchweightGoods(new CatchweightGoods("orange pickle","weird pickle"),2, pack);
        PackagedCatchweightGoods packProduct2 = new PackagedCatchweightGoods(new CatchweightGoods("gray pickle","weird pickle"),2, pack);
        PackagedCatchweightGoods packProduct3 = new PackagedCatchweightGoods(new CatchweightGoods("pink pickle","weird pickle"),2, pack);
        PackagedSetOfProduct packaged1 = new PackagedSetOfProduct(pack,"pickle box","big box pickle",packProduct1,packProduct2,packProduct3);

        PackagedCatchweightGoods packProduct4 = new PackagedCatchweightGoods(new CatchweightGoods("ice cream","yummy ice cream "),2, pack);
        PackagedCatchweightGoods packProduct5 = new PackagedCatchweightGoods(new CatchweightGoods("cool pickle","weird pickle"),2, pack);
        PackagedSetOfProduct packaged2 = new PackagedSetOfProduct(pack,"big box pickle box","something yummy",packaged1,packProduct4,packProduct5);

        Consignment a = new Consignment(packaged1,packaged2);
    }


//    @Test
//    public void countByFilterTest(){
//        ProductPackage pack = new ProductPackage("simple pack ", 1);
//        PackagedCatchweightGoods packProduct1 = new PackagedCatchweightGoods(new CatchweightGoods("orange pickle","weird pickle"),2, pack);
//        PackagedCatchweightGoods packProduct2 = new PackagedCatchweightGoods(new CatchweightGoods("gray pickle","weird pickle"),2, pack);
//        PackagedCatchweightGoods packProduct3 = new PackagedCatchweightGoods(new CatchweightGoods("pink pickle","weird pickle"),2, pack);
//        PackagedCatchweightGoods packProduct4 = new PackagedCatchweightGoods(new CatchweightGoods("ice cream","yummy ice cream "),2, pack);
//        PackagedCatchweightGoods packProduct5 = new PackagedCatchweightGoods(new CatchweightGoods("cool pickle","weird pickle"),2, pack);
//
//        PackagedSetOfProduct packaged = new PackagedSetOfProduct(pack,"pickle box","big box pickle",packProduct1,packProduct2,packProduct3,packProduct5);
//        PackagedSetOfProduct package2 = new PackagedSetOfProduct(pack,"big box pickle box","something yummy",packaged,packProduct2);
//        Consignment consignment = new Consignment(package2,packProduct1,packProduct2,packProduct3,packProduct4,packProduct5);
//        assertEquals(5, ServiceOfProduct.countByFilter(consignment,new ContainsStringFilter("pickle")));
//    }

   // @Ignore
//    @Test
//    public void countByFilterTestOnEmptyConsignment(){
//        Consignment consignment = new Consignment();
//        assertEquals(0,ServiceOfProduct.countByFilter(consignment,new ContainsStringFilter("pickle")));
//
//    }

//    @Test
//    public void countByFilterDeepTest(){
//
//        ProductPackage pack = new ProductPackage("simple pack ", 1);
//        PackagedCatchweightGoods packProduct1 = new PackagedCatchweightGoods(new CatchweightGoods("orange pickle","weird pickle"),2, pack);
//        PackagedCatchweightGoods packProduct2 = new PackagedCatchweightGoods(new CatchweightGoods("pink pickle","weird pickle"),2, pack);
//        PackagedCatchweightGoods packProduct3 = new PackagedCatchweightGoods(new CatchweightGoods("ice cream","yummy ice cream "),2, pack);
//        PackagedCatchweightGoods packProduct4 = new PackagedCatchweightGoods(new CatchweightGoods("blee pickle","simple pickle"),2,pack);
//        PackagedCatchweightGoods packProduct5 = new PackagedCatchweightGoods(new CatchweightGoods("simple pickle","simple pickle"),2,pack);
//
//
//        PackagedSetOfProduct packaged = new PackagedSetOfProduct(pack,"pickle box","big box pickle",packProduct3,packProduct1,packProduct2);
//        PackagedSetOfProduct package2 = new PackagedSetOfProduct(pack,"big box pickle box","something yummy",packaged,packProduct4);
//        Consignment consignment = new Consignment(package2,packaged,packProduct5);
//        assertEquals(3,ServiceOfProduct.countByFilterDeep(consignment,new ContainsStringFilter("pickle")));
//    }

//    @Ignore
//    @Test
//    public  void checkAllWeightTestTrue(){
//        Pack pack = new Pack("simple pack ", 1);
//        PackCashGoods packProduct1 = new PackCashGoods(new CashGoods("orange pickle","weird pickle"),2, pack);
//        PackCashGoods packProduct2 = new PackCashGoods( new CashGoods("pink pickle","weird pickle"),2, pack);
//        PackCashGoods packProduct3 = new PackCashGoods(new CashGoods("ice cream","yummy ice cream "),2, pack);
//        PackCashGoods packProduct4 = new PackCashGoods(new CashGoods("blee pickle","simple pickle"),2,pack);
//        PackCashGoods packProduct5 =new PackCashGoods(new CashGoods("simple pickle","simple pickle"),2,pack);
//
//
//        Consignment consignment = new Consignment(packProduct1,packProduct2,packProduct5);
//        assertTrue(ProductService.checkAllWeighted(consignment));
//
//    }
//
//    @Ignore
//    @Test
//    public  void checkAllWeightTestFalse(){
//        Pack pack = new Pack("simple pack ", 1);
//        PackCashGoods packProduct1 = new PackCashGoods(new CashGoods("orange pickle","weird pickle"),2, pack);
//        PackCashGoods packProduct2 = new PackCashGoods( new CashGoods("pink pickle","weird pickle"),2, pack);
//        PackCashGoods packProduct5 =new PackCashGoods(new CashGoods("simple pickle","simple pickle"),2,pack);
//        PackPieceGoods packProduct4 = new PackPieceGoods(new PieceGoods("keyboard","cool keyboard",12),2,pack);
//
//        Consignment consignment = new Consignment(packProduct1,packProduct2,packProduct5,packProduct4);
//        assertFalse(ProductService.checkAllWeighted(consignment));
//    }
//
//    @Ignore
//    @Test
//    public  void checkAllWeightTestEmptyConsignment(){
//        Consignment consignment = new Consignment();
//        assertTrue(ProductService.checkAllWeighted(consignment));
//    }
}
