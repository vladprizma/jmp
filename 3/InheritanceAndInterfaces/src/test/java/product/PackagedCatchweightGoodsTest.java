package product;

import PackageAndProduct.CatchweightGoods;
import PackageAndProduct.PackagedCatchweightGoods;
import PackageAndProduct.ProductPackage;
import org.junit.Test;

public class PackagedCatchweightGoodsTest {
    @Test(expected = IllegalArgumentException.class)
    public void constructorTest(){
            ProductPackage pack = new ProductPackage("cool", 15) {
                @Override
            public int GetPackageWeight() {
                return 0;
            }
        };
        CatchweightGoods cash = new CatchweightGoods("something","cool");
        PackageAndProduct.PackagedCatchweightGoods object = new PackageAndProduct.PackagedCatchweightGoods(cash,-12,pack);


    }
}
