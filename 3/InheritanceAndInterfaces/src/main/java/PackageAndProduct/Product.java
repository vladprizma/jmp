package PackageAndProduct;

import java.util.Objects;

public abstract class Product {
    private String name;
    private String description;

    public Product(String name, String description) {
        if ("".equals(name.trim())) {
            throw new IllegalArgumentException("Product can't be unnamed");
        }
        if ("".equals(description.trim())) {
            throw new IllegalArgumentException("Description cannot be empty");
        }
        this.name = name;
        this.description = description;

    }

    public Product(Product obj){
        this.name = obj.name;
        this.description = obj.description;
    }

    public abstract int getWeight();

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return name.equals(product.name) &&
                description.equals(product.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description);
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
