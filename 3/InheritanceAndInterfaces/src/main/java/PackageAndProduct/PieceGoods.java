package PackageAndProduct;
import java.util.Objects;

public class PieceGoods extends Product{
    int weight;

    public PieceGoods(String name, String description, int weight){
        super(name, description);
        if (weight <=0){
            throw new IllegalArgumentException("Package can't weight not natural number");
        }
        this.weight = weight;
    }


    public PieceGoods(PieceGoods obj){
        super(obj);
        this.weight = obj.weight;
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public String getName(){
        return super.getName();
    }

    @Override
    public String getDescription(){
        return super.getDescription();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PieceGoods that = (PieceGoods) o;
        return weight == that.weight && super.equals(that);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), weight);
    }

    @Override
    public String toString() {
        return "PieceGoods{" +
                super.toString() +
                "weight=" + weight +
                '}';
    }
}
