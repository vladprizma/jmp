package PackageAndProduct;

import java.util.Arrays;
import java.util.Objects;

public class PackagedSetOfProduct extends Product implements IPackedProduct {
    private ProductPackage pack;
    private IPackedProduct[] set;

    public PackagedSetOfProduct(ProductPackage pack, String name, String description, IPackedProduct... set) {
        super(name, description);
        this.pack = pack;
        this.set = set;
    }

    public IPackedProduct getProduct(int indx){
        return (IPackedProduct)set[indx];
    }

    public int getCount(){
        return set.length;
    }

    @Override
    public int getWeight() {
        int sum = 0;
        for (IPackedProduct p : set) {
            sum += p.getGrossMass();
        }
        return sum;
    }

    public int getLength(){
        return set.length;
    }

    @Override
    public int getGrossMass() {
        return pack.GetPackageWeight() + this.getWeight();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PackagedSetOfProduct that = (PackagedSetOfProduct) o;
        return Objects.equals(pack, that.pack) &&
                Arrays.equals(set, that.set);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(super.hashCode(), pack);
        result = 31 * result + Arrays.hashCode(set);
        return result;
    }

    @Override
    public String toString() {
        return "PackagedGoods{" +
                "pack=" + pack +
                ", set=" + Arrays.toString(set) +
                '}';
    }
}
