package PackageAndProduct;

import java.util.Objects;

public class PackagedCatchweightGoods extends CatchweightGoods implements IPackedProduct {
    private int weight;
    private ProductPackage pack;

    public PackagedCatchweightGoods(CatchweightGoods goods, int weight, ProductPackage pack) {
        super(goods);
        if (weight <= 0)
            throw new IllegalArgumentException("Package can't weight not natural number");
        this.weight = weight;
        this.pack = pack;
    }

    public PackagedCatchweightGoods(String name, String description) {
        super(name, description);
    }

    public PackagedCatchweightGoods(CatchweightGoods obj) {
        super(obj);
    }

    @Override
    public int getWeight() {
        return this.weight;
    }

    @Override
    public int getGrossMass() {
        return pack.GetPackageWeight() + this.getWeight();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PackagedCatchweightGoods that = (PackagedCatchweightGoods) o;
        return Objects.equals(weight, that.weight) &&
                Objects.equals(pack, that.pack);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), weight, pack);
    }

    @Override
    public String toString() {
        return "PackCashGoods{" +
                "weight=" + weight +
                ", package=" + pack +
                '}';
    }
}