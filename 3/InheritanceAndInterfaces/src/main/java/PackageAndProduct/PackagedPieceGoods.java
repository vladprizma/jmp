package PackageAndProduct;

import java.util.Objects;

public class PackagedPieceGoods extends PieceGoods implements IPackedProduct {
    private ProductPackage pack;
    private int amount;

    public PackagedPieceGoods(PieceGoods goods, int amount, ProductPackage pack) {
        super(goods);
        if (amount <= 0)
            throw new IllegalArgumentException("Amount of piece goods cannot be lower 0");
        this.amount = amount;
        this.pack = pack;
    }

    @Override
    public int getWeight() {
        return this.amount * super.getWeight();
    }

    @Override
    public int getGrossMass() {
        return pack.GetPackageWeight() + this.getWeight();
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PackagedPieceGoods that = (PackagedPieceGoods) o;
        return Objects.equals(pack, that.pack) &&
                Objects.equals(amount, that.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), pack, amount);
    }

    @Override
    public String toString() {
        return "PackPieceGoods{" +
                "pack=" + pack +
                ", amount=" + amount +
                ", weight=" + super.getWeight() +
                '}';
    }
}