package PackageAndProduct;

//Pack

import java.util.Objects;

public class ProductPackage {
    private String name;
    private int weight = 0;

    public ProductPackage( String name, int weight){
        if( "".equals(name.trim()) ){
            throw new IllegalArgumentException("Package can't be unnamed");
        }
        if (weight <=0){
            throw new IllegalArgumentException("Package can't weight not natural number");
        }
        this.name = name.trim();
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public int GetPackageWeight(){
        return weight;
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductPackage that = (ProductPackage) o;
        return weight == that.weight &&
                name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name,weight);
    }

    @Override
    public String toString() {
        return "ProductPackage{" +
                "name='" + name + '\'' +
                ", weight=" + weight +
                '}';
    }
}
//constructor
//getter setter
//equals
//hashCode
//toString