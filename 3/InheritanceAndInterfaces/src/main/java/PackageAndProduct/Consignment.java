package PackageAndProduct;

import java.util.Arrays;

public class Consignment {
    private IPackedProduct[] set;

    public Consignment(IPackedProduct... set) {
        this.set = set;
    }

    public int totalWeight() {
        int sum = 0;
        for (IPackedProduct p : set) {
            sum += p.getGrossMass();
        }
        return sum;
    }

    public IPackedProduct getProduct(int indx) {
        return (IPackedProduct) set[indx];
    }

    public int getCount() {
        return set.length;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Consignment that = (Consignment) o;
        return Arrays.equals(set, that.set);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(set);
    }

    @Override
    public String toString() {
        return "Consignment{" +
                "set=" + Arrays.toString(set) +
                '}';
    }
}
