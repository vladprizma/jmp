package PackageAndProduct;

public class CatchweightGoods extends Product{

    public CatchweightGoods(String name, String description){
        super(name,description);
    }

    public CatchweightGoods(CatchweightGoods obj){
        super(obj);
    }

    @Override
    public int getWeight() {
        return 0;
    }

    @Override
    public String getName(){
        return super.getName();
    }

    @Override
public String getDescription(){
        return super.getDescription();
        }

//Веса нет, смысла сравнивать не много

@Override
public String toString() {
        return "CatchweightGoods{" +
        super.toString() +
        '}';
        }
        }
