package Filter;


public class BeginStringFilter implements IFilter {
    private String pattern;

    public BeginStringFilter( String pattern){
        if(pattern.trim().equals(""))
            throw new IllegalArgumentException("Pattern cannot be empty");
        this.pattern = pattern;
    }

    public boolean apply(String str){
        return str.startsWith(pattern);
    }
}