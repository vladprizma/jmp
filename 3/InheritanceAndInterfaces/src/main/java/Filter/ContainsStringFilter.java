package Filter;



public class ContainsStringFilter implements IFilter {
    private String pattern;

    public ContainsStringFilter( String pattern){
        if(pattern.trim().equals(""))
            throw new IllegalArgumentException("Pattern cannot be empty");
        this.pattern = pattern;
    }

    public boolean apply( String str){
        return str.contains(pattern);
    }
}
