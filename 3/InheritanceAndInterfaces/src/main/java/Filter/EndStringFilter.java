package Filter;


public class EndStringFilter implements IFilter{
        private String pattern;

        public EndStringFilter( String pattern){
            if(pattern.trim().equals(""))
                throw new IllegalArgumentException("Pattern cannot be empty");
            this.pattern = pattern;
        }

        public boolean apply( String str){
            return str.endsWith(pattern);
        }
}
