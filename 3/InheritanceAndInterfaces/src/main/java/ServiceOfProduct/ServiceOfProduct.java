package ServiceOfProduct;
import Filter.IFilter;
import PackageAndProduct.*;

public class ServiceOfProduct {
        public static int countByFilter(Consignment party, IFilter filter) {
            int sum = 0;
            for (int i = 0; i < party.getCount(); i++) {
                if (filter.apply(party.getProduct(i).getName())) {
                    sum++;
                }
            }
            return sum;
        }

        public static int countByFilterDeep(Consignment party, IFilter filter){
            int sum = 0;
            for (int i = 0; i < party.getCount(); i++) {
                if (filter.apply(party.getProduct(i).getName())) {
                    sum++;
                }
                if(party.getProduct(i) instanceof PackagedSetOfProduct) {//+Рекурсия
                    for (int j = 0; j < ((PackagedSetOfProduct) party.getProduct(i)).getLength(); j++) {
                        if (((PackagedSetOfProduct) party.getProduct(i)).getProduct(j) instanceof PackagedSetOfProduct) {
                            //Запускаем рекурсию
                            sum += countByFilterDeep(new Consignment(( (PackagedSetOfProduct) party.getProduct(i) ).getProduct(j) ),filter );
                        }
                        if (((PackagedSetOfProduct) party.getProduct(i)).getProduct(j) instanceof PackagedCatchweightGoods) {
                            if (filter.apply(((PackagedSetOfProduct) party.getProduct(i)).getProduct(j).getName())) {
                                sum++;
                            }
                        }
                    }
                }
                //Варианты:
                //Закинуть SetOfProduct в consignment, но тогда придется СОЗДАВАТЬ объект, это съест память
                //Как тогда пройтись по массиву SetOfProduct
            }
            return sum;
        }

        public static boolean checkAllWeighted(Consignment party){
            for (int i = 0; i < party.getCount(); i++) {
                if (!(party.getProduct(i) instanceof PackagedCatchweightGoods)) {
                    return false;
                }
                if(party.getProduct(i) instanceof PackagedSetOfProduct) {//+Рекурсия
                    for (int j = 0; j < ((PackagedSetOfProduct) party.getProduct(i)).getLength(); j++) {//1
                        if(((PackagedSetOfProduct) party.getProduct(i)).getProduct(j) instanceof PackagedSetOfProduct){//2
                            if(checkAllWeighted(new Consignment(( (PackagedSetOfProduct) party.getProduct(i) ).getProduct(j) ))){
                             continue;
                            }
                            return false;
                        }
                        if (!(((PackagedSetOfProduct) party.getProduct(i)).getProduct(j) instanceof PackagedCatchweightGoods)) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
//    public static boolean checkAllWeighted(Consignment party){
//        for (int i = 0; i < party.getCount(); i++) {
//            if (!(party.getProduct(i) instanceof PackagedCatchweightGoods)) {
//                return false;
//            }
//            if(party.getProduct(i) instanceof PackagedSetOfProduct) {
//                for (int j = 0; j < ((PackagedSetOfProduct) party.getProduct(i)).getLength(); i++) {
//                    if (!(((PackagedSetOfProduct) party.getProduct(i)).getProduct(j) instanceof PackagedCatchweightGoods)) {
//                        return false;
//                    }
//                }
//            }
//        }
//        return true;
//    }
}
