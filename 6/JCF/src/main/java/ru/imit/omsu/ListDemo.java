package ru.imit.omsu;

import org.jetbrains.annotations.NotNull;
import ru.imit.omsu.Human;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ListDemo {
    public static List<Human> sameLastName(@NotNull List<Human> listHuman, Human hum){
        if(hum == null){
            throw new NullPointerException("Wrong data in collection");
        }
        List<Human> buf = new ArrayList<>();
        for(Human human : listHuman){
            if (human == null) {
                throw new NullPointerException("Wrong data in list");
            }
            if(human.getLastName().equals(hum.getLastName())){
                buf.add(human);
            }
        }
        return buf;
    }


    public static Set<Human> maxAge(@NotNull List<Human> listHuman){
        Set<Human> buf = new HashSet<>();
        int maximAge = 0;
        for(Human human : listHuman){
            if (human == null) {
                throw new NullPointerException("Wrong data in list");
            }
            if(human.getAge() > maximAge){
                maximAge = human.getAge();
            }
        }
        for(Human human : listHuman){
            if(human.getAge() == maximAge){
                buf.add(human.getExample());
            }
        }

        return buf;
    }
}
