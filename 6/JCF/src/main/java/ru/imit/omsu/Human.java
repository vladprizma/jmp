package ru.imit.omsu;

import java.util.Objects;

public class Human {
    private String lastName;//фамилия
    private String firstName;//имя
    private String patronymic;//отчество
    private int age;

    public Human(String lastName, String firstName, String patronymic, int age) {
        if ("".equals(lastName.trim()) || "".equals(firstName.trim()) || "".equals(patronymic.trim()) || age < 0){
            throw new IllegalArgumentException("Incorrect data");
        }
        this.lastName = lastName.trim();
        this.firstName = firstName.trim();
        this.patronymic = patronymic.trim();
        this.age = age;
    }

    public Human(final Human human) {
        this(human.lastName, human.firstName, human.patronymic, human.age);
    }

    public Human getExample(){
        return new Human(lastName, firstName, patronymic, age);
    }

    public String getLastName() { return lastName; }

    public String getFirstName() { return firstName; }

    public String getPatronymic() { return patronymic; }

    public int getAge() { return age; }

    public void setLastName(String lastName) {
        if ("".equals(lastName.trim())){
            throw new IllegalArgumentException("Incorrect data");
        }
        this.lastName = lastName.trim();
    }

    public void setFirstName(String firstName) {
        if ("".equals(firstName.trim())){
            throw new IllegalArgumentException("Incorrect data");
        }
        this.firstName = firstName.trim();
    }

    public void setPatronymic(String patronymic) {
        if ("".equals(patronymic.trim())){
            throw new IllegalArgumentException("Incorrect data");
        }
        this.patronymic = patronymic.trim();
    }

    public void setAge(int age) {
        if (age < 0){
            throw new IllegalArgumentException("Incorrect data");
        }
        this.age = age;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age &&
                Objects.equals(lastName, human.lastName) &&
                Objects.equals(firstName, human.firstName) &&
                Objects.equals(patronymic, human.patronymic);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastName, firstName, patronymic, age);
    }
}
