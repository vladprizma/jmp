package ru.imit.omsu;

import org.jetbrains.annotations.NotNull;

import java.util.*;

//добавить классы в свои пакеты(как и тесты)
public class CollectionsDemo {


    public static int amountOfWordsWithSameCharacter(@NotNull List<String> list, char character) {
        int buf = 0;
        for(String elem : list){
            if(elem == null || elem.equals("")){
                throw new NullPointerException("Wrong data in collection");
            }
            if (elem.charAt(0) == character) {
                buf++;
            }
        }

        return buf;
    }

    public static List<Human> listWithoutHuman(@NotNull List<Human> listHuman, @NotNull Human withoutHuman) {
        List<Human> buf = new ArrayList<>();
        for (Human human : listHuman) {
            if(human == null){
                throw new NullPointerException("Wrong data in collection");

            }
            if (!human.equals(withoutHuman)) {
                buf.add(human);
            }
        }
        return buf;
    }

    public static List<Set<Integer>> listOfSetsWithoutIntersection(@NotNull List<Set<Integer>> listSet, @NotNull Set<Integer> set) {
        if(set.contains(null)){
            throw new NullPointerException("Wrong data in collection");
        }
        List<Set<Integer>> buf = new ArrayList<>();
        boolean add = false;

        for (Set<Integer> setOfList : listSet){
            if(setOfList == null || setOfList.contains(null)){
                throw new NullPointerException("Wrong data in collection");
            }
            add = true;
            for(Integer num : set){
                if(setOfList.contains(num)){
                    add = false;
                    break;
                }
            }
            if(add){
                buf.add(setOfList);
            }
        }

        return buf;
    }

    public static Set<Human> mapping(@NotNull Map<Integer, Human> map, @NotNull Set<Integer> intSet) {
        if(map.containsKey(null)){
            throw new NullPointerException("Wrong data in collection");
        }
        if(intSet.contains(null)){
            throw new NullPointerException("Wrong data in collection");
        }
        Set<Human> buf = new HashSet<>();

        for (Integer key : map.keySet()) {
            Human human = map.get(key);
            if(human == null){
                throw new NullPointerException("Wrong data in collection");
            }
            if (intSet.contains(key)) {
                buf.add(human.getExample());
            }
        }
        return buf;
    }

    public static List<Integer> getOver18Age(@NotNull Map<Integer, Human> map) {
        if(map.containsKey(null)){
            throw new NullPointerException("Wrong data in collection");
        }
        List<Integer> buf = new ArrayList<>();

        for (Integer key : map.keySet()) {
            Human human = map.get(key);
            if(human == null){
                throw new NullPointerException("Wrong data in collection");
            }
            if (human.getAge() >= 18) {
                buf.add(key);
            }
        }

        return buf;
    }

    public static Map<Integer, Integer> convertMapIdentifierAge(@NotNull Map<Integer, Human> map) {
        Map<Integer, Integer> buf = new HashMap<Integer, Integer>();

        for (Integer key : map.keySet()) {
            Human human = map.get(key);
            if(human == null){
                throw new NullPointerException("Wrong data in collection");
            }
            buf.put(key, human.getAge());
        }

        return buf;
    }

    public static Map<Integer, List<Human>> createMap(@NotNull Set<Human> humanSet) {
        if(humanSet.contains(null)){
            throw new NullPointerException("Wrong data in collection");
        }
        Map<Integer, List<Human>> buf = new HashMap<Integer, List<Human>>();
        int ageBuf;

        for (Human human : humanSet) {
            ageBuf = human.getAge();

            if (!buf.containsKey(ageBuf)) {
                buf.put(ageBuf, new ArrayList<>());
            }
            buf.get(ageBuf).add(human);
        }

        return buf;
    }


}