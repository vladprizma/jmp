package ru.imit.omsu;

import java.util.Objects;

public class Student extends Human {
    private String faculty;

    public Student(String lastName, String firstName, String patronymic, int age, String faculty) {
        super(lastName, firstName, patronymic, age);
        if ("".equals(faculty.trim())){
            throw new IllegalArgumentException("Incorrect data");
        }
        this.faculty = faculty;
    }

    public Student(final Student student) {
        super(student);
        if ("".equals(student.faculty.trim())){
            throw new IllegalArgumentException("Incorrect data");
        }
        this.faculty = student.faculty;
    }

    @Override
    public Student getExample(){
        return new Student(getLastName(), getFirstName() ,getPatronymic(), getAge(), faculty);
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        if ("".equals(faculty.trim())){
            throw new IllegalArgumentException("Incorrect data");
        }
        this.faculty = faculty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Student student = (Student) o;
        return Objects.equals(faculty, student.faculty);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), faculty);
    }
}
