package ru.imit.omsu;

import org.junit.Test;
import ru.imit.omsu.CollectionsDemo;
import ru.imit.omsu.Human;
import ru.imit.omsu.Student;

import java.util.*;

import static org.junit.Assert.*;

public class CollectionsDemoTest {

    @Test(expected = NullPointerException.class)
    public void sameCharacterException(){
        ArrayList<String> test = new ArrayList<>();
        test.add("");
        test.add(null);
        test.add("APPLE");
        test.add("AXE");
        test.add("BANANA");
        test.add("ARROW");
        char testCh = 'A';

        CollectionsDemo.amountOfWordsWithSameCharacter(test,testCh);
    }

    @Test
    public void sameCharacter(){
        ArrayList<String> test = new ArrayList<>();
        test.add("APPLE");
        test.add("AXE");
        test.add("BANANA");
        test.add("ARROW");
        char testCh = 'A';

        assertEquals(3,CollectionsDemo.amountOfWordsWithSameCharacter(test,testCh));
    }

    @Test(expected = NullPointerException.class)
    public void listWithoutHumanException(){
        ArrayList<Human> test = new ArrayList<>();
        test.add(new Human("A","B","C",27));
        test.add(new Human("B","B","C",30));
        test.add(new Human("A","B","C",34));
        test.add(null);

        Human testHuman = new Human("B","B","C",30);

        ArrayList<Human> testAnswer = new ArrayList<>();
        testAnswer.add(new Human("A","B","C",27));
        testAnswer.add(new Human("A","B","C",34));

        CollectionsDemo.listWithoutHuman(test,testHuman);
    }

    @Test
    public void listWithoutHuman(){
        ArrayList<Human> test = new ArrayList<>();
        test.add(new Human("A","B","C",27));
        test.add(new Human("B","B","C",30));
        test.add(new Human("A","B","C",34));

        Human testHuman = new Human("B","B","C",30);

        ArrayList<Human> testAnswer = new ArrayList<>();
        testAnswer.add(new Human("A","B","C",27));
        testAnswer.add(new Human("A","B","C",34));

        assertEquals(testAnswer,CollectionsDemo.listWithoutHuman(test,testHuman));
    }

    @Test(expected = NullPointerException.class)
    public void intersectionTestException(){

        HashSet<Integer> set1 = new HashSet<>();
        HashSet<Integer> set2 = new HashSet<>();
        HashSet<Integer> set3 = null;
        HashSet<Integer> set4 = new HashSet<>();
        HashSet<Integer> set = new HashSet<>();
        Collections.addAll(set1, 1,2,3);
        Collections.addAll(set2, 1,2,4);
        Collections.addAll(set3, 5,6);
        //Collections.addAll(set4, 7, null);
        Collections.addAll(set, 4,5);

        List<Set<Integer>> expected = new ArrayList<>();
        Collections.addAll(expected,set1,set4 );

        List<Set<Integer>> actual = new ArrayList<>();
        Collections.addAll(actual,set1,set2,set3,set4 );

        CollectionsDemo.listOfSetsWithoutIntersection(actual, set);

    }

    @Test
    public void intersection(){

        HashSet<Integer> set1 = new HashSet<>();
        HashSet<Integer> set2 = new HashSet<>();
        HashSet<Integer> set3 = new HashSet<>();
        HashSet<Integer> set4 = new HashSet<>();
        HashSet<Integer> set = new HashSet<>();
        Collections.addAll(set1, 1,2,3);
        Collections.addAll(set2, 1,2,4);
        Collections.addAll(set3, 5,6);
        Collections.addAll(set4, 7,6);
        Collections.addAll(set, 4,5);

        List<Set<Integer>> expected = new ArrayList<>();
        Collections.addAll(expected,set1,set4 );

        List<Set<Integer>> actual = new ArrayList<>();
        Collections.addAll(actual,set1,set2,set3,set4 );

        assertEquals(expected,CollectionsDemo.listOfSetsWithoutIntersection(actual, set));

    }

    @Test
    public void mapping(){
        Map<Integer,Human> test = new HashMap<>();
        test.put(0, new Human("A","B","C",27));
        test.put(1,new Human("B","B","C",30));
        test.put(2,new Human("A","B","C",34));//3
        test.put(4,new Student("A","G","C",30,"IMIT"));//4
        //Если будет 2 у студента, то запись в test обновится с ru.imit.omsu.Human(//3) на ru.imit.omsu.Student(//4)

        HashSet<Integer> testSet = new HashSet<>();
        testSet.add(2);
        testSet.add(4);
        testSet.add(5);

        HashSet<Human> testAnswer = new HashSet<>();
        testAnswer.add(new Human("A","B","C",34));
        testAnswer.add(new Student("A","G","C",30,"IMIT"));

        assertEquals(testAnswer,CollectionsDemo.mapping(test,testSet));
    }

    @Test
    public void testGetOver18Age(){
        Map<Integer,Human> test = new HashMap<>();
        test.put(0, new Human("A","B","C",2));
        test.put(1,new Human("B","B","C",13));
        test.put(2,new Human("A","B","C",34));//3
        test.put(4,new Student("A","G","C",30,"IMIT"));


        ArrayList<Integer> testAnswer = new ArrayList<>();
        testAnswer.add(2);
        testAnswer.add(4);

        assertEquals(testAnswer,CollectionsDemo.getOver18Age(test));
    }

    @Test
    public void testConvertMapIdentifierAge(){
        Map<Integer,Human> test = new HashMap<>();
        test.put(0, new Human("A","B","C",27));
        test.put(1,new Human("B","B","C",30));
        test.put(2,new Human("A","B","C",34));
        test.put(4,new Student("A","G","C",30,"IMIT"));


        Map<Integer,Integer> testAnswer = new HashMap<>();
        testAnswer.put(0, 27);
        testAnswer.put(1, 30);
        testAnswer.put(2, 34);
        testAnswer.put(4, 30);

        assertEquals(testAnswer,CollectionsDemo.convertMapIdentifierAge(test));
    }

    @Test
    public void testCreateMap(){
        Set<Human> test = new HashSet<>();
        test.add( new Human("A","B","C",27));
        test.add(new Human("B","B","C",30));
        test.add(new Human("A","B","C",30));
        test.add(new Student("A","G","C",27,"IMIT"));

        ArrayList<Human> test1 = new ArrayList<>();
        test1.add(new Human("A","B","C",27));
        test1.add(new Student("A","G","C",27,"IMIT"));

        ArrayList<Human> test2 = new ArrayList<>();
        test2.add(new Human("B","B","C",30));
        test2.add(new Human("A","B","C",30));

        Map<Integer,List<Human>> testAnswer = new HashMap<>();
        testAnswer.put(27, test1);
        testAnswer.put(30, test2);

        assertEquals(testAnswer,CollectionsDemo.createMap(test));
    }


}