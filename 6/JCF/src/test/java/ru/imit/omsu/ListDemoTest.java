package ru.imit.omsu;

import org.junit.Test;
import ru.imit.omsu.Human;
import ru.imit.omsu.ListDemo;
import ru.imit.omsu.Student;

import java.util.ArrayList;
import java.util.HashSet;

import static org.junit.Assert.*;

public class ListDemoTest {

    @Test
    public void sameLastName(){
        ArrayList<Human> test = new ArrayList<>();
        test.add(new Human("A","B","C",27));
        test.add(new Human("B","B","C",30));
        test.add(new Human("A","B","C",34));

        Human testHuman = new Human("A","B","D",20);

        ArrayList<Human> testAnswer = new ArrayList<>();
        testAnswer.add(new Human("A","B","C",27));
        testAnswer.add(new Human("A","B","C",34));

        assertEquals(testAnswer, ListDemo.sameLastName(test,testHuman));
    }

    @Test
    public void maxAge(){
        ArrayList<Human> test = new ArrayList<>();
        test.add(new Human("A","B","P",17));
        test.add(new Human("B","V","E",21));
        test.add(new Human("F","J","C",20));
        test.add(new Human("G","R","M",21));
        test.add(new Student("A","B","C",21,"IMIT"));

        HashSet<Human> testAnswer = new HashSet<>();
        testAnswer.add(new Human("B","V","E",21));
        testAnswer.add(new Human("G","R","M",21));
        testAnswer.add(new Student("A","B","C",21,"IMIT"));

        assertEquals(testAnswer,ListDemo.maxAge(test));
    }

    @Test
    public void maxAge2(){
        ArrayList<Human> test = new ArrayList<>();
        test.add(new Human("A","B","P",17));
        test.add(new Human("B","V","E",21));
        test.add(new Human("F","J","C",20));
        test.add(new Student("A","B","C",21,"IMIT"));

        HashSet<Human> testAnswer = new HashSet<>();
        testAnswer.add(new Human("B","V","E",21));
        testAnswer.add(new Student("A","B","C",21,"IMIT"));

        assertEquals(testAnswer,ListDemo.maxAge(test));
    }

}