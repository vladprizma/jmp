package Equations;

import static java.lang.Math.sqrt;

public class AxBxC {
    private static final double EPS = 1E-9;
    private double A,B,C;

    public AxBxC(double A, double B, double C) throws IllegalArgumentException{
        if(Math.abs(A) < EPS){
            throw new IllegalArgumentException("This is will not be quadratic equation!");
        }
        this.A = A;
        this.B = B;
        this.C = C;
    }

   public double getA() {
        return A;
    }

    public double getB() {
        return B;
    }

    public double getC() {
        return C;
    }

    //Поаккуратней с даблом, не сравнивать через ==
    public double[] solveEquation(){
        double x1, x2;//Корни
        double D;//Дискриминант

        //Дискриминант равен нулю когда многочлен имеет кратные корни
        D = B * B - 4 * A * C;
        if (D < -EPS) {
            //2x^2 - 6x + 5 = 0
            //3/2 - 1/2*i - 1 корень
            //3/2 + 1/2*i - 2 корень
            //throw new IllegalArgumentException("Only two imaginary roots detected");
            return new double[0];
        }
        //Два вещественных корня
        if (D > EPS) {
            //x^2 + 2x - 8 = 0
            //2 - 1 корень
            //-4 - 2 корень
            x1 = (-B + sqrt(D)) / (2 * A);
            x2 = (-B - sqrt(D)) / (2 * A);
            return new double[]{x1,x2};
        }
        if (D < EPS) {
            x1 = -B / (2 * A);
            return new double[]{x1};
        }

        return new double[]{};
    }
}
