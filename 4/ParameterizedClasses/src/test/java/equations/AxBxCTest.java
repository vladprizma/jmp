package Equations;

import org.junit.Test;

import static org.junit.Assert.*;

public class AxBxCTest {

    @Test
    public void solveEquation() {
        AxBxC dBiggerThanZero = new AxBxC(1 , 2 , -8);
        //(D>0)
        //x^2 + 2x - 8 = 0
        //2 - 1 корень
        //-4 - 2 корень
        assertArrayEquals(new double[]{2, -4}, dBiggerThanZero.solveEquation(), 0.000001);
    }
}