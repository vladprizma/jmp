package Functions;

//A*x + B
public class FirstFunc implements IFunction {
    private static final double EPS = 1E-9;
    private double a;
    private double b;
    private double left;
    private double right;

    public FirstFunc(double a, double b, double left, double right) throws FunctionsException {
        if(left-right >= EPS){
            throw new FunctionsException("Wrong borders");
        }
        this.a = a;
        this.b = b;
        this.left = left;
        this.right = right;
    }

    public double getValue(double x) throws FunctionsException {
        if((x - left < EPS) || (x - right > EPS)){
            throw new FunctionsException("Variable out of function borders");
        }
        return a * x + b;
    }

    public double getLeftBorder() {
        return this.left;
    }

    public double getRightBorder() {
        return this.right;
    }

}

