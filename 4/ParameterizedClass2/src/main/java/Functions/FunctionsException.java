package Functions;

public class FunctionsException extends Exception {
    public FunctionsException(String msg){
        super(msg);
    }
}
