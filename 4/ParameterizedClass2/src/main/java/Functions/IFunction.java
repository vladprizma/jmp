package Functions;

public interface IFunction {

    double getValue(double x) throws FunctionsException;

    double getLeftBorder();

    double getRightBorder();

}
