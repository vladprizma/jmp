package Functions;

//(A*x + B) / (C*x + D)
public class ThirdFunc implements IFunction {
    private static final double EPS = 1E-9;
    private double a;
    private double b;
    private double c;
    private double d;
    private double left;
    private double right;

    public ThirdFunc(double a, double b, double c, double d, double left, double right) throws FunctionsException {
        if(left-right >= EPS){
            throw new FunctionsException("Wrong borders");
        }
        if(c == 0 && d == 0){
            throw new FunctionsException("Zero division prevention");
        }
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.left = left;
        this.right = right;
    }

    public double getValue(double x) throws FunctionsException {
        if((x - left < EPS) || (x - right > EPS)){
            throw new FunctionsException("Variable out of function borders");
        }
        if(c*x == 0 && d == 0){
            throw new FunctionsException("Zero division prevention");
        }
        return (a *x + b)/(c*x + d);
    }

    public double getLeftBorder() {
        return this.left;
    }

    public double getRightBorder() {
        return this.right;
    }

}
