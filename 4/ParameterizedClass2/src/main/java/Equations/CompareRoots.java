package Equations;

import Equations.AxBxC;

public class CompareRoots{
    private AxBxC test;

    public CompareRoots(AxBxC test) {
        this.test = new AxBxC(test.getA(),test.getB(), test.getC());
    }

    public double compare(){

        double [] buf = test.solveEquation();

        if(buf.length == 0){
            throw new IllegalArgumentException("No real roots found");
        }
        if (buf.length == 2) {
            if (buf[1] >= buf[0]) {
                return buf[1];
            }
        }

        return buf[0];

    }

}
