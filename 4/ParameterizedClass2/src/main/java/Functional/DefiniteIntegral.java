package Functional;

import Functions.FunctionsException;
import Functions.IFunction;

public class DefiniteIntegral<F extends IFunction> implements IFunctional<F>  {
    private static final double EPS = 1E-9;
    private double left;
    private double right;
    private double n;

    public DefiniteIntegral(double left, double right, double n) throws FunctionsException {
        if(left-right >= EPS){
            throw new FunctionsException("Wrong borders");
        }
        if(n < EPS){
            throw new FunctionsException("Not valid partition");
        }
        this.left = left;
        this.right = right;
        this.n = n;
    }

    private double integral(F function, double step) throws FunctionsException {
        double integral = 0;

        for(double i = left+step; i - right < -EPS; i+=step) {
            integral+= function.getValue(i + step*0.5)*step;
        }
        return integral;
    }

    public double getValue(F function) throws FunctionsException {
        if( left-function.getLeftBorder() < EPS || right - function.getRightBorder() > EPS ){
            throw new FunctionsException("Integral borders out of function borders");
        }
        if(Math.abs(left-right) < EPS){
            return 0;
        }

        double step = (right-left)/n;
        double integral = integral(function,step);

        return integral;
    }
}
