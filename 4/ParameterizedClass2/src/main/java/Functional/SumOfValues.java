package Functional;

import Functions.FunctionsException;
import Functions.IFunction;

public class SumOfValues<F extends IFunction> implements IFunctional<F> {
    private static final double EPS = 1E-9;
    private double left;
    private double right;

    public SumOfValues(double left, double right) throws FunctionsException {
        if(left-right >= EPS){
            throw new FunctionsException("Wrong borders");
        }
        this.left = left;
        this.right = right;
    }

    public double getValue(F function) throws FunctionsException {
        return function.getValue(left) +
                function.getValue((left+right)/2) +
                function.getValue(right);
    }

}
