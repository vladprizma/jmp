package Functional;

import Functions.FunctionsException;
import Functions.IFunction;

public interface IFunctional<F extends IFunction> {

    double getValue(F function) throws FunctionsException;

}
