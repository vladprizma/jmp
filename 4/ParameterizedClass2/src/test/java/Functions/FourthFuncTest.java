package Functions;

import org.junit.Test;

import static org.junit.Assert.*;

//A*exp(x) + B
public class FourthFuncTest {

    @Test(expected = FunctionsException.class)
    public void getValue() throws FunctionsException {
        //FourthFunc test = new FourthFunc(1,1, 1,0);
        FourthFunc test = new FourthFunc(1,1, 0,1);
        test.getValue(2);

    }

    @Test
    public void getValue2() throws FunctionsException {
        FourthFunc test4 = new FourthFunc(1,0,-1 , 5);

        assertEquals(Math.E,test4.getValue(1),0.0001);
    }

    @Test(expected = FunctionsException.class)
    public void getValue3() throws FunctionsException {
        FourthFunc test = new FourthFunc(1,1, 3,1);

    }

}