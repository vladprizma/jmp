package Functions;

import org.junit.Test;

import static org.junit.Assert.*;

//(A*x + B) / (C*x + D)
public class ThirdFuncTest {

    @Test(expected = FunctionsException.class)
    public void getValue() throws FunctionsException {
        ThirdFunc test3 = new ThirdFunc(1,1, 1,0 , -1 ,1);
        test3.getValue(0);

    }

    @Test
    public void getValue2() throws FunctionsException {
        ThirdFunc test4 = new ThirdFunc(5,10, 3,2 , -1 , 1);

        assertEquals(3,test4.getValue(1),0.0001);
    }

    @Test(expected = FunctionsException.class)
    public void getValue3() throws FunctionsException {
        ThirdFunc test2 = new ThirdFunc(1,1, 1,1 , 0 ,1);
        test2.getValue(2);
    }

    @Test(expected = FunctionsException.class)
    public void getValue4() throws FunctionsException {
        ThirdFunc test = new ThirdFunc(1,1, 1,1 , 1,0);
    }

}