package Functions;

import org.junit.Test;
import static org.junit.Assert.*;

//A*x + B
public class FirstFuncTest {

    @Test(expected = FunctionsException.class)
    public void getValue() throws FunctionsException {
        FirstFunc test = new FirstFunc(1,1, 1,0);

        FirstFunc test2 = new FirstFunc(1,1, 0,1);
        test2.getValue(2);
    }

    @Test(expected = FunctionsException.class)
    public void getValue2() throws FunctionsException {
        FirstFunc test3 = new FirstFunc(5,5, -1,2);

        assertEquals(15.5,test3.getValue(2.1),0.0001);
    }

    @Test
    public void getValue3() throws FunctionsException {
        FirstFunc test3 = new FirstFunc(5,5, -5,0);

        assertEquals(5,test3.getValue(0),0.0001);
    }

}