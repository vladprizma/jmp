package Functions;

import org.junit.Test;

import static org.junit.Assert.*;

//A*sin(Bx)
public class SecondFuncTest {

    @Test(expected = FunctionsException.class)
    public void getValue() throws FunctionsException {
        SecondFunc test = new SecondFunc(1,1, 1,0);

    }

    @Test
    public void getValue2() throws FunctionsException {
        SecondFunc test3 = new SecondFunc(5,1, -1,5);

        assertEquals(5,test3.getValue(Math.PI/2),0.0001);
    }

    @Test(expected = FunctionsException.class)
    public void getValue3() throws FunctionsException {
        SecondFunc test = new SecondFunc(1,1, 0,4);

        test.getValue(5);
    }

}