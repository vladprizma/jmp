package Equations;

import org.junit.Test;

import static org.junit.Assert.*;

public class CompareRootsTest {
    //x^2 + 2x - 8 = 0
    //2 - 1 корень
    //-4 - 2 корень

    //2x^2 - 6x + 5 = 0
    //3/2 - 1/2*i - 1 корень
    //3/2 + 1/2*i - 2 корень

    //3x^2 - 18x + 27 = 0
    // 3 - 1 корень
    @Test
    public void getValue() {
        AxBxC test = new AxBxC(1 , 2 , -8);
        CompareRoots test2 = new CompareRoots(test);

        assertEquals(2,test2.compare(),0.0001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getValue2() {
        AxBxC test = new AxBxC(2 , -6 , 5);
        CompareRoots test2 = new CompareRoots(test);
        test2.compare();
    }

    @Test
    public void getValue3() {
        AxBxC test = new AxBxC(3 , -18 , 27);
        CompareRoots test2 = new CompareRoots(test);

        assertEquals(3,test2.compare(),0.0001);
    }

}