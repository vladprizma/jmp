package Functional;

import Functions.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class DefiniteIntegralTest {

    @Test(expected = FunctionsException.class)
    public void getValue() throws FunctionsException {
        FirstFunc test = new FirstFunc(1,0,-10,10);
        DefiniteIntegral<FirstFunc> test2 = new DefiniteIntegral<FirstFunc>(-5,11, 1);
        test2.getValue(test);
    }

    @Test(expected = FunctionsException.class)
    public void incorrectPartition() throws FunctionsException {
        FirstFunc test = new FirstFunc(1,0,-10,10);
        DefiniteIntegral<FirstFunc> test2 = new DefiniteIntegral<FirstFunc>(-5,11, 1E-10);
    }

    @Test(expected = FunctionsException.class)
    public void incorrectBorders() throws FunctionsException {
        FirstFunc test = new FirstFunc(1,0,1,10);
        DefiniteIntegral<FirstFunc> test2 = new DefiniteIntegral<FirstFunc>(12,11, 1E-8);
    }

    //A*x + B
    @Test
    public void getValueFirstFunc() throws FunctionsException {
        FirstFunc test = new FirstFunc(2,3,-20,20);
        DefiniteIntegral<FirstFunc> test2 = new DefiniteIntegral<FirstFunc>(1,2, 100);

        assertEquals(6 ,test2.getValue(test),0.1);
    }

    //A*sin(Bx)
    @Test
    public void getValueSecondFunc() throws FunctionsException {
        SecondFunc test = new SecondFunc(2,1,-20,20);
        DefiniteIntegral<SecondFunc> test2 = new DefiniteIntegral<SecondFunc>(1,2, 1000);

        assertEquals(2 ,test2.getValue(test),0.1);
    }

    //(A*x + B) / (C*x + D)
    @Test
    public void getValueThirdFunc() throws FunctionsException {
        ThirdFunc test = new ThirdFunc(2,3, 2, 4,-20,20);
        DefiniteIntegral<ThirdFunc> test2 = new DefiniteIntegral<ThirdFunc>(1,2, 100);

        assertEquals(0.856 ,test2.getValue(test),0.1);
    }

    //A*exp(x) + B
    @Test
    public void getValueFourthFunc() throws FunctionsException {
        FourthFunc test = new FourthFunc(2,3,-20,20);
        DefiniteIntegral<FourthFunc> test2 = new DefiniteIntegral<FourthFunc>(1,2, 100);

        assertEquals(12.341 ,test2.getValue(test),0.1);
    }

}