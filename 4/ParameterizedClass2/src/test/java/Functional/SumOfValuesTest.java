package Functional;

import Functions.FirstFunc;
import Functions.FunctionsException;
import org.junit.Test;

import static org.junit.Assert.*;

public class SumOfValuesTest {

    @Test(expected = FunctionsException.class)
    public void getValue() throws FunctionsException {
        FirstFunc test = new FirstFunc(1,0,-10,10);
        SumOfValues<FirstFunc> test2 = new SumOfValues<FirstFunc>(7,5);
        test2.getValue(test);
    }

    //A*x + B
    @Test
    public void getValue2() throws FunctionsException {
        FirstFunc test = new FirstFunc(1,0,-10,10);
        SumOfValues<FirstFunc> test2 = new SumOfValues<FirstFunc>(-1,9);

        assertEquals(12 ,test2.getValue(test),0.0001);
    }
}