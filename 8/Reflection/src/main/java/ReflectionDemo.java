import Example.Human;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class ReflectionDemo {

    public static int countHumanAndDerivatives(List<?> arr){
        int buf = 0;
        for(Object elem : arr) {
            if (elem instanceof Human) {
                buf++;
            }
        }
        return buf;
    }

    public static List<String> getPublicMethods(Object obj){
        List<String> buf = new ArrayList<>();
        for(Method elem: obj.getClass().getMethods()){
            buf.add(elem.getName());
        }
        return buf;
    }

    public static List<String> getSuperClasses(Object obj){
        List<String> buf = new ArrayList<>();
        Class<?> objCheck = obj.getClass();
        while(objCheck != Object.class){
            buf.add(objCheck.getSuperclass().getName());
            objCheck = objCheck.getSuperclass();
        }
        return buf;
    }

}
