import Example.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.*;

public class ReflectionDemoTest {

    @Test
    public void testHuman(){
        List<Object> test = new ArrayList<Object>();
        Collections.addAll(test,
                new Test1(),
                new Test2(),
                new Human(),
                new Student(),
                new Teacher()
        );
        assertEquals(3, ReflectionDemo.countHumanAndDerivatives(test));
    };

    @Test
    public void testGetMethods(){
        List<String> test = new ArrayList<>();
        Collections.addAll(test,"humanTest", "humanTestTwo",
                "wait", "wait", "wait", "equals", "toString",
                "hashCode", "getClass", "notify", "notifyAll");
        test.sort(Comparator.naturalOrder());
        List<String> test2 = ReflectionDemo.getPublicMethods(new Human());
        test2.sort(Comparator.naturalOrder());
        assertEquals(test, test2);
    };

    @Test
    public void testGetSuperClasses(){
        List<String> test = new ArrayList<>();
        Collections.addAll(test,"Example.Human", "java.lang.Object");
        assertEquals(test, ReflectionDemo.getSuperClasses(new Teacher()));
    };


}